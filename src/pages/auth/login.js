import React, { Component } from 'react'
import { Input, Button, message } from 'antd'
import { Formik, Field } from 'formik'
import * as yup from 'yup'
import './style.scss'
import styled from 'styled-components'

import { Container, FitHeightContainer, Page } from 'app/components'
import Storage from 'app/utils/storage'
import { login } from 'app/api/auth'
import { Images } from 'app/theme'

const Logo = styled.img`
  width: 100px;
  border-radius: 50%;
  margin: 0 auto;
  transition: 0.5s;

  &:hover {
    opacity: 0.5;
  }

  &:active {
    width: 150px;
  }
`

const schemaValidation = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required()
})

class Login extends Component {
  state = {
    isLoading: false
  }

  componentDidMount() {
    document.title = 'Seminar - Login'
  }

  _onSubmit = async (values) => {
    try {
      this.setState({
        isLoading: true
      })

      const result = await login(values)

      const { history } = this.props

      Storage.set('ACCESS_TOKEN', `${result.type} ${result.token}`)
      Storage.set('IS_ADMIN', result.role === 1)

      history.push(result.role === 1 ? '/admin' : '/')
    } catch (e) {
      message.error('Email or password incorrect!')
    }

    this.setState({
      isLoading: false
    })
  }

  render() {
    const { isLoading } = this.state

    return (
      <Page className="login">
        <FitHeightContainer>
          <Container>
            <Formik
              validationSchema={schemaValidation}
              onSubmit={this._onSubmit}
              render={props => (
                <form className="form" onSubmit={props.handleSubmit}>
                  <Logo src={Images.CAT_IMAGE} />
                  <h1>Seminar Managerment</h1>
                  <div className="field">
                    <p className="label">Email</p>
                    <Field
                      name="email"
                      render={({ field, form }) => (
                        <div>
                          <Input {...field} />
                          <p className="error-message">{form.errors.email}</p>
                        </div>
                      )}
                    />
                  </div>
                  <div className="field">
                    <p className="label">Password</p>
                    <Field
                      name="password"
                      render={({ field, form }) => (
                        <div>
                          <Input {...field} type="password" />
                          <p className="error-message">{form.errors.password}</p>
                        </div>
                      )}
                    />
                  </div>
                  <Button
                    onClick={props.handleSubmit}
                    className="button-login"
                    type="primary"
                    loading={isLoading}
                  >
                    Login
                  </Button>
                </form>
              )}
            />
          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}

export default Login
