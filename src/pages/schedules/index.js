import React, { Component } from 'react'
// import { Table, Button } from 'antd'

import { Container, FitHeightContainer, Page } from 'app/components'


class Schedules extends Component {
  render() {

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h1>SCHEDULES</h1>
          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}

export default Schedules
