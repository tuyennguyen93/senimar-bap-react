import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Table, Button, Popconfirm, Pagination } from 'antd'
import { Container, FitHeightContainer, Page } from 'app/components'
import { actions } from 'app/store/actions'

import EditUserModal from './edit-user-modal'

const PAGE_SIZE = 6

@connect(state => ({
  usersStore: state.users
}), {
  getUsers: actions.getUsers,
  createUser: actions.createUser,
  updateUser: actions.updateUser,
  deleteUser: actions.deleteUser
})

class Users extends Component {
  componentDidMount() {
    this.props.getUsers({
      page: 1,
      pageSize: PAGE_SIZE
    })
  }

  _onDeleteUser = (user) => {
    this.props.deleteUser(user)
  }

  _onPageChange = (page) => {
    this.props.getUsers({
      page,
      pageSize: PAGE_SIZE
    })
  }

  render() {
    const { usersStore, createUser, updateUser } = this.props

    const columns = [{
      title: 'No',
      dataIndex: 'no',
      key: 'no',
      render: (row, record, index) => index + 1
    }, {
      title: 'Name',
      dataIndex: 'name',
      key: 'name'
    }, {
      title: 'Email',
      dataIndex: 'email',
      key: 'email'
    }, {
      title: 'Role',
      dataIndex: 'role',
      key: 'role',
      render: row => (row === 1 ? 'Admin' : 'User')
    }, {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      render: (row, record) => (
        <div>
          <Button
            onClick={() => this._editUserModal.open(record)}
          >
            Update
          </Button>
          <Popconfirm
            title="Are you sure delete this user?"
            onConfirm={() => this._onDeleteUser(record)}
            okText="Yes"
            cancelText="No"
          >
            <Button
              onClick={this._}
            >
              Delete
            </Button>
          </Popconfirm>
        </div>
      )
    }]

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h1>USERS</h1>
            <Button
              onClick={() => this._editUserModal.open()}
            >
              Add user
            </Button>
            <Table
              rowKey={(row, index) => index}
              pagination={false}
              columns={columns}
              dataSource={usersStore.users.content}
            />
            <Pagination
              defaultCurrent={1}
              pageSize={PAGE_SIZE}
              total={usersStore.users.totalElements}
              onChange={this._onPageChange}
            />
            <EditUserModal
              usersStore={usersStore}
              createUser={createUser}
              updateUser={updateUser}
              ref={(ref) => { this._editUserModal = ref }}
            />
          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}

export default Users
