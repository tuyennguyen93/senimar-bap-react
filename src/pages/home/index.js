import React, { Component } from 'react'
// import { Table, Button } from 'antd'
import { connect } from 'react-redux'
import './style.scss'

import { Container, FitHeightContainer, Page } from 'app/components'
import { actions } from 'app/store/actions'
// import EditTopicModal from './edit-topic-modal'

@connect(state => ({
  topicsStore: state.topics
}), {
  getAllTopics: actions.getAllTopics
})

class TopicsWithRedux extends Component {
  componentDidMount() {
    // this.props.getAllTopics()
  }

  render() {
    // const { topics, loading } = this.props.topicsStore
    //
    // const columns = [{
    //   title: 'Name',
    //   dataIndex: 'name',
    //   key: 'name'
    // }, {
    //   title: 'Description',
    //   dataIndex: 'description',
    //   key: 'description'
    // }, {
    //   title: 'Author',
    //   dataIndex: 'author',
    //   key: 'author'
    // }]

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            {/* <h2>Topic List (using Redux)</h2>
            <Button
              onClick={() => {
                this._editTopicModal.open()
              }}
            >
              Add Topic
            </Button>
            <Table
              rowKey={(row, index) => index}
              onRow={row => ({
                onClick: () => {
                  this._editTopicModal.open(row)
                }
              })}
              pagination={false}
              loading={loading}
              dataSource={topics}
              columns={columns}
            />
            <EditTopicModal
              ref={(ref) => { this._editTopicModal = ref }}
            /> */}
          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}

export default TopicsWithRedux
