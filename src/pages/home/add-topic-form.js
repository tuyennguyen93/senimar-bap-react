import React, { Component } from 'react'
import { Input, Button } from 'antd'
import { connect } from 'react-redux'
import { Formik, Field, Form } from 'formik'
import * as yup from 'yup'

import { actions } from 'app/store/actions'

const schemaValidation = yup.object().shape({
  name: yup.string().required(),
  author: yup.string().required(),
  authorEmail: yup.string().email().required()
})

@connect(state => ({
  topicsStore: state.topics
}), {
  createTopic: actions.createTopic
})

class AddTopicForm extends Component {
  _onSubmit = (values) => {
    const { createTopic } = this.props

    createTopic(values)
  }

  render() {
    return (
      <div className="add-topic-form">
        <Formik
          validationSchema={schemaValidation}
          onSubmit={this._onSubmit}
          render={({ handleSubmit }) => (
            <Form>
              <div className="field">
                <p className="label">Name</p>
                <Field
                  name="name"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">{form.errors.name}</p>
                    </div>
                  )}
                />
              </div>
              <div className="field">
                <p className="label">Author</p>
                <Field
                  name="author"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">{form.errors.author}</p>
                    </div>
                  )}
                />
              </div>
              <div className="field">
                <p className="label">Author Email</p>
                <Field
                  name="authorEmail"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">{form.errors.authorEmail}</p>
                    </div>
                  )}
                />
              </div>
              <div className="field">
                <p className="label">Description</p>
                <Field
                  name="description"
                  render={({ field, form }) => (
                    <div>
                      <Input {...field} />
                      <p className="error-message">{form.errors.description}</p>
                    </div>
                  )}
                />
              </div>
              <Button onClick={handleSubmit}>Add</Button>
            </Form>
          )}
        />
      </div>
    )
  }
}

export default AddTopicForm
