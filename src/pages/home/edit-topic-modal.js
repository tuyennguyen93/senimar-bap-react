import React from 'react'
import { Modal, Button, Input, Row, Col, Select } from 'antd'

class EditTopicModal extends React.Component {
  state = {
    visible: false,
    topic: null
  }

  open = (topic) => {
    this.setState({
      visible: true,
      topic
    })
  };

  close = e => {
    this.setState({
      visible: false,
      topic: null
    })
  }

  _onSelectChange = (value) => {
    console.log(value);
  }

  render() {
    const { topic } = this.state
    console.log(topic);
    return (
      <div>
        <Modal
          title={topic ? 'Update Topic' : 'Add Topic'}
          visible={this.state.visible}
          onCancel={this.close}
        >
          <Row>
            <Col sm={24} lg={10}>
              <Input value={topic?.name}></Input>
              <Input value={topic?.description}></Input>
              <Input value={topic?.author}></Input>
            </Col>
            <Col sm={24} lg={14}>
              <Input value={topic?.name}></Input>
              <Input value={topic?.description}></Input>
              <Input value={topic?.author}></Input>
            </Col>
          </Row>
          <Select
            mode="multiple"
            style={{ width: '100%' }}
            placeholder="Please select"
            onChange={this._onSelectChange}
          >
            <Select.Option value='Vietnam'>Vietnam</Select.Option>
            <Select.Option value='Lao'>Lao</Select.Option>
            <Select.Option value='Campuchia'>Campuchia</Select.Option>
          </Select>,
        </Modal>
      </div>
    )
  }
}

export default EditTopicModal
