import React, { Component } from 'react'
// import { Table, Button } from 'antd'

import { Container, FitHeightContainer, Page } from 'app/components'


class Topics extends Component {
  render() {

    return (
      <Page className="home">
        <FitHeightContainer hasHeader>
          <Container>
            <h1>TOPICS</h1>
          </Container>
        </FitHeightContainer>
      </Page>
    )
  }
}

export default Topics
