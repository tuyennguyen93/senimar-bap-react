import React, { Component } from 'react'
import { connect } from 'react-redux'
import './style.scss'

import { actions } from 'app/store/actions'

@connect(state => ({
  topicsStore: state.topics
}), {
  getTopicDetails: actions.getTopicDetails
})

class TopicDetails extends Component {
  componentDidMount() {
    this.props.getTopicDetails({
      id: this.props.match.params.id
    })
  }

  render() {
    const { topicsStore } = this.props

    return (
      <div className="topic-details">
        <div className="field">
          <p className="label">Name</p>
          <div className="value">{topicsStore.currentTopic?.name}</div>
        </div>
        <div className="field">
          <p className="label">Description</p>
          <div className="value">{topicsStore.currentTopic?.description}</div>
        </div>
        <div className="field">
          <p className="label">Author</p>
          <div className="value">{topicsStore.currentTopic?.author}</div>
        </div>
        <div className="field">
          <p className="label">Rating</p>
          <div className="value">{topicsStore.currentTopic?.rating}</div>
        </div>
        <img src={topicsStore.currentTopic?.image} alt="" />
      </div>
    )
  }
}

export default TopicDetails
