import lodash from 'lodash'
import { Toaster, Position, Intent } from '@blueprintjs/core'

const toaster = Toaster.create({
  position: Position.TOP_CENTER
})

function isFetchError(error) {
  return !!error && lodash.hasIn(error, 'status') && lodash.isFunction(error.json)
}

async function getFetchError(error) {
  try {
    return await error.json()
  } catch (e) {
    return null
  }
}

class Notification {
  static success(options) {
    return this._show(options, Intent.SUCCESS, 'tick')
  }

  static warning(options) {
    return this._show(options, Intent.WARNING)
  }

  static danger(options) {
    return this._show(options, Intent.DANGER, 'error')
  }

  static async error(error = null, onFetched) {
    let text

    if (error) {
      const originalError = error

      if (isFetchError(error)) {
        error = await getFetchError(error)
      }

      /* eslint-disable no-console */
      console.error('[notification] error:', error || originalError)

      let message
      if (lodash.isFunction(onFetched)) {
        message = onFetched(error)
      } else if (lodash.isString(error)) {
        message = error
      } else {
        message = error && (error.message || error.error || error.errors)
      }

      if (lodash.isString(message)) {
        text = message
      }
    }

    return this.danger(text)
  }

  static info(options) {
    return this._show(options, Intent.PRIMARY)
  }

  static show(options) {
    return toaster.show(options)
  }

  static _show(options, intent, icon) {
    if (lodash.isString(options)) {
      options = { message: options }
    }

    options = { icon, ...options, intent }

    return toaster.show(options)
  }
}

export default Notification
