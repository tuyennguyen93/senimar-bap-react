export default [
  'GET_ALL_TOPICS',
  'GET_TOPIC_DETAILS',
  'CREATE_TOPIC',

  'GET_USERS',
  'CREATE_USER',
  'UPDATE_USER',
  'DELETE_USER'
]
