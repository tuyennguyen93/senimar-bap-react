import { TYPES } from 'app/store/actions'
import lodash from 'lodash'

const INIT_STATE = {
  submitting: null,
  users: []
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case TYPES.GET_USERS_REQUEST:
    case TYPES.CREATE_USER_REQUEST:
    case TYPES.UPDATE_USER_REQUEST:
    case TYPES.DELETE_USER_REQUEST:
      return {
        ...state,
        submitting: action.type
      }
    case TYPES.GET_USERS_SUCCESS:
      return {
        ...state,
        submitting: null,
        users: action.data
      }
    case TYPES.CREATE_USER_SUCCESS:
      return {
        ...state,
        submitting: null,
        users: [action.payload].concat(state.users)
      }
    case TYPES.UPDATE_USER_SUCCESS:
      const index = lodash.findIndex(state.users, item => item.id === action.payload.id)
      return {
        ...state,
        submitting: null,
        users: state.users.slice(0, index).concat([action.payload]).concat(state.users.slice(index + 1, state.users.length))
      }
    case TYPES.DELETE_USER_SUCCESS:
      return {
        ...state,
        submitting: null,
        users: state.users.filter(item => item.id !== action.payload.id)
      }
    case TYPES.GET_USERS_FAILURE:
    case TYPES.CREATE_USER_FAILURE:
    case TYPES.UPDATE_USER_FAILURE:
    case TYPES.DELETE_USER_FAILURE:
      return {
        ...state,
        submitting: null
      }
    default:
      return state
  }
}
