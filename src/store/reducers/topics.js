import { TYPES } from 'app/store/actions'

const INIT_STATE = {
  loading: false,
  topics: [],
  currentTopic: null
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case TYPES.GET_ALL_TOPICS_REQUEST:
    case TYPES.GET_TOPIC_DETAILS_REQUEST:
    case TYPES.CREATE_TOPIC_REQUEST:
      return {
        ...state,
        loading: true
      }
    case TYPES.GET_ALL_TOPICS_SUCCESS:
      return {
        ...state,
        loading: false,
        topics: action.data
      }
    case TYPES.GET_TOPIC_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        currentTopic: action.data
      }
    case TYPES.CREATE_TOPIC_SUCCESS:
      return {
        ...state,
        loading: false,
        topics: [action.payload].concat(state.topics)
      }
    case TYPES.GET_ALL_TOPICS_FAILURE:
    case TYPES.GET_TOPIC_DETAILS_FAILURE:
    case TYPES.CREATE_TOPIC_FAILURE:
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}
