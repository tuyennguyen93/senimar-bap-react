import { all } from 'redux-saga/effects'

import topics from './topics'
import users from './users'

export default function* sagas() {
  yield all([
    topics(),
    users()
  ])
}
