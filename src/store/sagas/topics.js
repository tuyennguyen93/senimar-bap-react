import { all, takeLatest } from 'redux-saga/effects'

import sagaHelper from 'app/utils/saga-helper'
import { TYPES } from 'app/store/actions'

import { getAllTopics, getTopicDetails, createTopic } from 'app/api/topics'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_ALL_TOPICS, sagaHelper({
      api: getAllTopics
    })),
    takeLatest(TYPES.GET_TOPIC_DETAILS, sagaHelper({
      api: getTopicDetails
    })),
    takeLatest(TYPES.CREATE_TOPIC, sagaHelper({
      api: createTopic
    }))
  ])
}
