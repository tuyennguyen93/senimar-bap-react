import { all, takeLatest } from 'redux-saga/effects'

import sagaHelper from 'app/utils/saga-helper'
import { TYPES } from 'app/store/actions'

import { getUsers, createUser, updateUser, deleteUser } from 'app/api/users'

export default function* watcher() {
  yield all([
    takeLatest(TYPES.GET_USERS, sagaHelper({
      api: getUsers
    })),
    takeLatest(TYPES.CREATE_USER, sagaHelper({
      api: createUser
    })),
    takeLatest(TYPES.UPDATE_USER, sagaHelper({
      api: updateUser
    })),
    takeLatest(TYPES.DELETE_USER, sagaHelper({
      api: deleteUser
    }))
  ])
}
