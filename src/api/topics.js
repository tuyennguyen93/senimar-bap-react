import { MainApi } from './endpoint'

export function getAllTopics() {
  return MainApi.get('/topics')
}

export function getTopicDetails(data) {
  return MainApi.get('/topic-details', data)
}

export function createTopic(data) {
  return MainApi.post('/create-topic', data)
}
