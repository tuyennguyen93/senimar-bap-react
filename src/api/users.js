import { MainApi } from './endpoint'

export function getUsers(payload) {
  return MainApi.get('/users', payload)
}

export function createUser(payload) {
  return MainApi.post('/users', payload)
}

export function updateUser({ id, ...payload }) {
  return MainApi.put(`/users/${id}`, payload)
}

export function deleteUser({ id }) {
  return MainApi.delete(`/users/${id}`)
}
