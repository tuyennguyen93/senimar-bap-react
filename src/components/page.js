import React from 'react'
import styled from 'styled-components'
import classNames from 'classnames'

const Div = styled.div`
  overflow-y: auto;
  overflow-x: hidden;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 67px;
`

export default function ({ children, toolbar, className, triangeStyle, ...props }) {
  return (
    <>
      <Div
        {...props}
        className={classNames(toolbar && 'with-toolbar', className)}
      >
        {children}
      </Div>
    </>
  )
}
